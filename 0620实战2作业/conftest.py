
"""
'''
理解 conftest.py
在当前目录下创建一个conftest.py文件
1、conftest.py 文件不需要导入，直接引用 里面定义的fixture方法
2、就近原则， 当前模块> 当前目录的conftest.py > 父级目录的conftest.py>祖辈目录（不会找兄弟结点）
3、一点来说，conftest.py会存放一些公共的方法，比如fixture, 还有hook
'''

"""
from typing import List
import yaml
import logging
import allure
import pytest

#获取测试数据
def get_calc_data():
    with open('./datas/datas1.yaml') as f:
        totals = yaml.safe_load(f)
    # return totals['datas'],totals['ids']
    # reture totals
    add_int_datas = totals['add']['int']['datas']
    add_int_ids = totals['add']['int']['ids']

    add_float_datas=totals['add']['float']['datas']
    add_float_ids = totals['add']['float']['ids']

    add_abnormal_datas = totals['add']['abnormal']['datas']
    add_abnormal_ids = totals['add']['abnormal']['ids']

    div_int_datas = totals['div']['int']['datas']
    div_int_ids = totals['div']['int']['ids']

    div_float_datas = totals['div']['float']['datas']
    div_float_ids = totals['div']['float']['ids']

    div_abnormal_datas = totals['div']['abnormal']['datas']
    div_abnormal_ids = totals['div']['abnormal']['ids']


    return (add_int_datas,add_int_ids,add_float_datas,add_float_ids,add_abnormal_datas,add_abnormal_ids,div_int_datas,div_int_ids, div_float_datas,div_float_ids,div_abnormal_datas,div_abnormal_ids)

#验证返回数据
def test_getdatas():
    print(get_calc_data()[11] )
#获取计算机实例的fixture
from pythoncode.calculator import Calculator

@pytest.fixture(scope='class')    #有这个几不需要setup、tearndown
def get_calc_object():
    # print("开始计算")
    logging.info(f'开始计算')
    calc=Calculator()             #创建calc实例
    yield calc                    #返回实例，如果只需要返回不需要用后面的可以用return
    logging.info(f'结束计算')
    # print("结束计算")


@pytest.fixture(params=get_calc_data()[0],ids=get_calc_data()[1])
def get_addIntDatas(request):
    return request.param

@pytest.fixture(params=get_calc_data()[2],ids=get_calc_data()[3])
def get_addFloatDatas(request):
    return request.param

@pytest.fixture(params=get_calc_data()[4],ids=get_calc_data()[5])
def get_addAbnormal(request):
    return request.param

@pytest.fixture(params=get_calc_data()[6],ids=get_calc_data()[7])
def get_divIntDatas(request):
    return request.param

@pytest.fixture(params=get_calc_data()[8],ids=get_calc_data()[9])
def get_divFloatDatas(request):
    return request.param

@pytest.fixture(params=get_calc_data()[10],ids=get_calc_data()[11])
def get_divAbnormal(request):
    return request.param



# 改写 pytest_collection_modifyitems hook 函数，
# 收集上来所有的测试用例之后，修改items的方法
# 一般hook 会放在conftest.py 文件中，
# def pytest_collection_modifyitems(
#         session: "Session", config: "Config", items: List["Item"]
# ) -> None:
#     print("items ===>", items)
#     for item in items:
#         # item.name 测试用例的名字
#         # item.nodeid 测试用例的路径
#         # print(item.name)
#         # print(item.nodeid)
#         # 修改测试用例的编码
#         item.name = item.name.encode('utf-8').decode('unicode-escape')
#         item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')
#         if 'hook' in item.name:
#             item.add_marker(pytest.mark.hook)
#         if 'aaa' in item.name:
#             item.add_marker(pytest.mark.aaa)
