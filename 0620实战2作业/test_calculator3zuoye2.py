"""
___author__='guoyabing'
__time__='2021/6/22 16:52:50'
1.改造 计算器 测试用例，使用 fixture 函数获取计算器的实例

2.计算之前打印开始计算，计算之后打印结束计算

3.添加用例日志，并将日志保存到日志文件目录下

4.生成测试报告，展示测试用例的标题，用例步骤，与测试日志，截图附到课程贴下
"""
# 引入
import logging

import allure
import pytest

from testing.conftest import get_calc_data


@allure.feature('计算器')
class TestCalculator:
    @allure.story('相加int')
    @allure.title('相加_{get_addIntDatas[0]}_{get_addIntDatas[1]}')
    def test_add(self, get_calc_object, get_addIntDatas):
        with allure.step(f'步骤1获取数据相加_{get_addIntDatas[0]}_{get_addIntDatas[1]}'):
            logging.info(f'步骤1获取数据相加_{get_addIntDatas[0]}_{get_addIntDatas[1]}')
        with allure.step('步骤2'):
            logging.info('步骤2')
        logging.info(f'test_add 数据:{get_addIntDatas}')
        assert get_addIntDatas[2] == get_calc_object.add(get_addIntDatas[0], get_addIntDatas[1])

    @allure.story('相加float')
    @allure.title('相加_{get_addFloatDatas[0]}_{get_addFloatDatas[1]}')
    def test_add1(self, get_calc_object, get_addFloatDatas):
        assert get_addFloatDatas[2] == round((get_addFloatDatas[0] + get_addFloatDatas[1]), 2)

    @allure.story('相加Abnormal')
    @allure.title('相加_{get_addAbnormal[0]}_{get_addAbnormal[1]}')
    def test_add2(self, get_calc_object, get_addAbnormal):
        with pytest.raises(TypeError):
            assert get_addAbnormal[2] == get_calc_object.add(get_addAbnormal[0], get_addAbnormal[1])

    @allure.story('相除int')
    @allure.title('相除_{get_divIntDatas[0]}_{get_divIntDatas[1]}')
    # @pytest.mark.run(order=0)
    def test_div(self, get_calc_object, get_divIntDatas):
        # logging.info(f'test_add 数据:{get_divIntDatas}')
        assert get_divIntDatas[2] == round((get_divIntDatas[0] / get_divIntDatas[1]), 2)

    @allure.story('相除float')
    @allure.title('相除_{get_divFloatDatas[0]}_{get_divFloatDatas[1]}')
    def test_div1(self, get_calc_object, get_divFloatDatas):
        assert get_divFloatDatas[2] == round((get_divFloatDatas[0] / get_divFloatDatas[1]), 2)

    @allure.story('相除Abnormal')
    @allure.title('相除_{get_divAbnormal[0]}_{get_divAbnormal[1]}')
    def test_div2(self, get_calc_object, get_divAbnormal):
        with pytest.raises((ZeroDivisionError, TypeError)):
            assert get_divAbnormal[2] == get_calc_object.div(get_divAbnormal[0], get_divAbnormal[1])
