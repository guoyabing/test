# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/13 16:32
@Author  : guoyabing
@FileName: maploxal.py
@Software: PyCharm
 
'''

# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/13 15:10
@Author  : guoyabing
@FileName: rewrite.py
@Software: PyCharm

'''
import json

import mitmproxy.http
from mitmproxy import http, ctx


class Maplocal1:
    def request(self, flow: mitmproxy.http.HTTPFlow):
        # 给定监听的URL匹配规则
        if "https://stock.xueqiu.com/v5/stock/batch/quote.json?_t=" in flow.request.pretty_url \
                and "x=" in flow.request.pretty_url:
            # 打开本地文件
            with open("quest.json", encoding="utf-8") as f:
                # 制造响应体
                flow.response = http.HTTPResponse.make(
                    200,
                    f.read(),
                )


addons = [
    Maplocal1()
]

if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump

    # 使用debug模式启动mitmdump
    mitmdump(['-p', '8080', '-s', __file__])
