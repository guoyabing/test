# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/13 15:10
@Author  : guoyabing
@FileName: rewrite.py
@Software: PyCharm
 
'''
import json

import mitmproxy.http
from mitmproxy import http, ctx


class Rewrite:
    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
        """
        # 给定监听的URL匹配规则
        if "https://stock.xueqiu.com/v5/stock/batch/quote.json?_t=" in flow.request.pretty_url \
                and "x=" in flow.request.pretty_url:
            # 打印请求发出去后的响应体内容
            ctx.log.info(f"{flow.response.text}")
            ctx.log.info(str(type(flow.response.text)))
            # 使用json的loads方法转为字典格式
            data = json.loads(flow.response.text)
            #修改字典中的指定位置的数据，实现rewrite
            # data["data"]["items"][0]["quote"]["name"] = "guoyabing1"
            # data["data"]["items"][1]["quote"]["name"] = "guoyabing2"
            # data["data"]["items"][2]["quote"]["name"] = "guoyabing3"
            # data["data"]["items"][3]["quote"]["name"] = "guoyabing4"
            data["data"]["items"][0]["quote"]["current"] = "0.0000000000001"
            data["data"]["items"][1]["quote"]["current"] = "-0.0000000000001"
            data["data"]["items"][2]["quote"]["current"] = "999999999999999"
            data["data"]["items"][3]["quote"]["current"] = "-9999999999999"
            #替换响应体中的正文
            flow.response.text = json.dumps(data)

addons = [
    Rewrite()
]

if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump

    # 使用debug模式启动mitmdump
    mitmdump(['-p', '8080', '-s', __file__])
