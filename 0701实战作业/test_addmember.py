# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/2 16:06
@Author  : guoyabing
@FileName: test_addmember.py
@Software: PyCharm
 
'''
# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/2 15:02
@Author  : guoyabing
@FileName: test_daka.py
@Software: PyCharm

 前提条件
        已登录状态（ noReset=True）
        打卡用例：
        1、打开【企业微信】应用
        2、进入【工作台】
        3、点击【打卡】
        4、选择【外出打卡】tab
        5、点击【第N次打卡】
        6、验证【外出打卡成功】
        7、退出【企业微信】应用
        :return:
'''
from time import sleep

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
import pytest
import allure

@allure.feature('企业微信')
class TestWeiXin:
    def setup(self):
        #资源准备，打开应用
        caps = {}
        caps["platformName"] = "Android"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        caps["deviceName"] = "hogwarts"
        caps["noReset"] = "true"
        #以下可以不加，因为安卓默认
        caps["automationName"] = "uiautomator2"

        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        #隐式等待 5s动态得等待元素出现，如果5s之内都没有找到就报异常
        self.driver.implicitly_wait(5)

    def trardown(self):
        #资源的回收
        self.driver.quit()

    @allure.story('添加成员')
    @allure.title('手动添加成员')
    @pytest.mark.parametrize("username,phone", [("小1", "18800000002"), ("小2", "18800000003")])
    def test_daka(self,username,phone):
        with allure.step('点击通讯录'):
            self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']").click()
        with allure.step('点击添加成员,成员太多时，添加成员不在当前页面需要滚动查询'):
            self.driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,
                                 'new UiScrollable(new UiSelector().scrollable(true).\
                                 instance(0)).scrollIntoView(new UiSelector().\
                                 text("添加成员").instance(0));').click()
        with allure.step('验证'):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='微信邀请同事']")
        with allure.step('手动输入添加'):
            self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
        with allure.step('输入姓名手机号点击确定'):
            self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/b09").send_keys(username)
            self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/f7y").send_keys(phone)
            self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/ad2").click()
        with allure.step('toast控件识别和验证'):
            # print(self.driver.page_source)
            self.driver.find_element(MobileBy.XPATH,"//*[@text='添加成功']")



