# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/21 14:28
@Author  : guoyabing
@FileName: conftest.py
@Software: PyCharm
 
'''
import threading
import time

import pytest
from faker import Faker


@pytest.fixture()
def get_userid():
    # 获取时间戳
    userid = str(time.time()) + threading.currentThread().name
    return userid


@pytest.fixture()
def get_name():
    fake = Faker('zh_CN')
    name = fake.name()
    return name


@pytest.fixture()
def get_mobile():
    fake = Faker('zh_CN')
    mobile = fake.phone_number()
    return mobile
