# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/20 17:17
@Author  : guoyabing
@FileName: test_user.py
@Software: PyCharm
 
'''
import logging

import requests

from test_zuoye.api.user import User


class TestUser:

    def setup_class(self):
        self.user = User()
        print(dir(self.user))

    #新增成员
    def test_create_user(self, get_userid, get_name, get_mobile):
        new_user = self.user.create_user(get_userid, get_name, get_mobile).json()
        assert new_user.get('errcode') == 0
        assert self.user.is_in_userlist(get_userid)

    # 查询指定成员
    def test_get_user(self,get_userid,get_name, get_mobile):
        new_user = self.user.create_user(get_userid, get_name, get_mobile).json()
        r = self.user.get_user(get_userid)
        assert r.json().get('errcode') == 0
        print(r.json())

    # 修改成员
    def test_update_user(self,get_userid, get_name, get_mobile):
        new_user = self.user.create_user(get_userid, get_name, get_mobile).json()
        r = self.user.update_user(get_userid,"xiugai",get_mobile)
        assert r.json().get('errcode') == 0
    #删除成员
    def test_delete_user(self,get_userid,get_name, get_mobile):
        new_user = self.user.create_user(get_userid, get_name, get_mobile).json()
        r = self.user.delete_user(get_userid)
        assert r.json().get('errcode') == 0



