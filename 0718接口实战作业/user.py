# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/20 17:10
@Author  : guoyabing
@FileName: user.py
@Software: PyCharm
 
'''
import requests
from faker import Faker

from test_zuoye.api.base_api import BaseApi


class User(BaseApi):


    #添加成员
    def create_user(self,userid,name,mobile):
        data = {
            "userid": userid,
            "name": name,
            "mobile": mobile,
            "department": [1, 2],

        }
        r = requests.post(self.BASE_URL+f"create?access_token={self.token}",json=data)
        return r

    #读取成员
    def get_user(self,userid):
        r = requests.get(self.BASE_URL+f"get?access_token={self.token}&userid={userid}")
        return r

    def update_user(self,userid,name,mobile):
        data = {
            "userid": userid,
            "name": name,
            "mobile": mobile,
            "department": [1, 2],

        }
        r = requests.post(self.BASE_URL+f"update?access_token={self.token}", json=data)
        return r

    def delete_user(self,userid):
        r = requests.get(self.BASE_URL+f"delete?access_token={self.token}&userid={userid}")
        return r

    def get_list(self):
        r = requests.get(self.BASE_URL+f"simplelist?access_token={self.token}&department_id= 2&fetch_child=0")
        return r

    def is_in_userlist(self,user_id):
        user_list = self.get_list().json().get("userlist")
        for userid in user_list:
            if user_id == userid.get("userid"):
                return True
        return False





