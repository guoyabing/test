

'''
@Time    : 2021/6/25 17:18
@Author  : guoyabing
@FileName: test_web0625.py
@Software: PyCharm
 
'''
import yaml
from selenium import webdriver
import pytest
import  time
import allure
#chrome.exe --remote-debugging-port=9222
class TestWeb():

    def test_remote(self):
        #复用浏览器
        opt = webdriver.ChromeOptions()
        # opt.debugger_address = "127.0.0.1:9222"
        opt.add_experimental_option('debuggerAddress', '127.0.0.1:9222')
        driver = webdriver.Chrome(options=opt)
        driver.get("https://work.weixin.qq.com/wework_admin/frame")
        with open('./cookie.yml','w',encoding='utf-8') as f:
            print(driver.get_cookies())
            yaml.safe_dump(driver.get_cookies(),f)

    def test_login(self):
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.get('https://work.weixin.qq.com/')
        with open('./cookie.yml', 'r', encoding='utf-8') as f:
            cookies = yaml.safe_load(f)
        for cookie in cookies:
            driver.add_cookie(cookie)
        driver.get("https://work.weixin.qq.com/wework_admin/frame")
        with allure.step("点击通讯录"):
            driver.find_element_by_id("menu_contacts").click()
            time.sleep(3)
        with allure.step("打开添加成员"):
            driver.find_element_by_xpath('//*[@class="ww_operationBar"]/a[contains(@class,"js_add_member")]').click()
        with allure.step("填写信息"):
            time.sleep(3)
            driver.find_element_by_id("username").send_keys("name1")
            driver.find_element_by_id("memberAdd_acctid").send_keys("memberAdd_acctid1")
            driver.find_element_by_id("memberAdd_phone").send_keys("18883281732")
            driver.find_element_by_xpath('//div[contains(@class,"member_edit_formWrap")]/preceding-sibling::div/a[contains(@class,"js_btn_save")]').click()
            time.sleep(2)



