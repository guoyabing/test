# -*- coding: utf-8 -*-

'''
@Time    : 2021/6/29 18:19
@Author  : guoyabing
@FileName: test_add_member.py
@Software: PyCharm
 
'''
import pytest

from PO.main_page import MainPage


class TestAddMember:
    def setup(self):
        self.main = MainPage()

    def teardown(self):
     self.main.close_driver()

    @pytest.mark.parametrize("username,acctid,phone",[("张三","0630001","18800000000"),("李四","0630002","18800000001")])
    def test_add_member(self,username,acctid,phone):
        result = self.main.goto_contact().click_add_member().edit_member(username,acctid,phone).get_member_name()
        assert username in result
