# -*- coding: utf-8 -*-

'''
@Time    : 2021/6/29 16:39
@Author  : guoyabing
@FileName: add_menber_page.py
@Software: PyCharm
 
'''
#from PO.cantact_page import ContactPage 加在这里会报错的，E   ImportError: cannot import name 'ContactPage' from partially initialized module 'PO.cantact_page' (most likely due to a circular import) (D:\shizhanProject\PO\cantact_page.py)
#上面错误是循环导入，要解决用局部引入


#添加成员页面
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

from PO.base_page import BasePage


class AddMemberPage(BasePage):
    _NAME=(By.ID,"username")
    _MEMEBERADD=(By.ID,"memberAdd_acctid")
    _MEMEBERADDPHONE=(By.ID,"memberAdd_phone")
    #添加成员
    def edit_member(self,username,acctid,phone):
        #局部导入解决循环导入问题
        from PO.cantact_page import ContactPage
        # opt = webdriver.ChromeOptions()
        # opt.debugger_address = "127.0.0.1:9222"
        # self.driver = webdriver.Chrome(options=opt)
        self.driver.implicitly_wait(10)
        # time.sleep(1)
        # self.driver.find_element_by_id("username").send_keys("name1")
        # self.driver.find_element_by_id("memberAdd_acctid").send_keys("memberAdd_acctid1")
        # self.driver.find_element_by_id("memberAdd_phone").send_keys("18883281732")
        # self.driver.find_element_by_css_selector(".js_btn_save").click()
        self.find(*self._NAME).send_keys(username)
        self.find(*self._MEMEBERADD).send_keys(acctid)
        self.find(*self._MEMEBERADDPHONE).send_keys(phone)
        self.find_and_click(By.CSS_SELECTOR,".js_btn_save")
        return ContactPage(self.driver)