# -*- coding: utf-8 -*-

'''
@Time    : 2021/6/29 16:38
@Author  : guoyabing
@FileName: main_page.py
@Software: PyCharm
 
'''
from selenium import webdriver
from selenium.webdriver.common.by import By

from PO.base_page import BasePage
from PO.cantact_page import ContactPage

#第一步：主页跳转通讯录
class MainPage(BasePage):
    _CONTACT=(By.ID,"menu_contacts") #_约定的私有变量的方式

    def goto_contact(self):
        # opt = webdriver.ChromeOptions()
        # # opt.debugger_address = "127.0.0.1:9222"
        # opt.add_experimental_option('debuggerAddress', '127.0.0.1:9222')
        # self.driver = webdriver.Chrome(options=opt)
        # self.driver.implicitly_wait(10)
        # self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # self.driver.find_element_by_id("menu_contacts").click()
        #下面为封装之后
        self.find_and_click(*self._CONTACT) #加*解元组，拆开


        return ContactPage(self.driver)