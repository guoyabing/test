# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:33
@Author  : guoyabing
@FileName: app.py
@Software: PyCharm
 
'''
#启动、关闭、重启app
from appium import webdriver

from test_appium.zuoye2.page.main_page import MainPage
from test_appium.zuoye2.page.base_page import BasePage


class App(BasePage):
    def start(self):
        if self.driver==None:
            #启动应用
            print("driver==None 创建driver")
            caps = {}
            caps["platformName"] = "Android"
            caps["appPackage"] = "com.tencent.wework"
            caps["appActivity"] = ".launch.LaunchSplashActivity"
            caps["deviceName"] = "hogwarts"
            caps["noReset"] = "true"
            # 以下可以不加，因为安卓默认
            caps["automationName"] = "uiautomator2"

            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
            # 隐式等待 5s动态得等待元素出现，如果5s之内都没有找到就报异常
            self.driver.implicitly_wait(5)
        else:
            print("driver != None 复用driver")
            #launch_app帮助启动driver中的应用，不需要传递参数，省去了和server建立链接的步骤
            #start_activity可以启动任何应用，可以传递包名和应用 self.driver.start_activity()
            self.driver.launch_app()
        return self
    def restart(self):
        self.driver.close()
        self.driver.launch_app()
    def quit(self):
        self.driver.quit()
    def goto_main(self):
        #进入主页 入口
        return MainPage(self.driver)