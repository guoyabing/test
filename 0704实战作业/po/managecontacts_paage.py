# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/6 17:10
@Author  : guoyabing
@FileName: managecontacts_paage.py
@Software: PyCharm
 
'''
from appium.webdriver.common.mobileby import MobileBy

from test_appium.zuoye2.page.base_page import BasePage



class ManageContacts(BasePage):
    _members_ele = (MobileBy.XPATH, '//*[@text="企业通讯录"]/../following-sibling::android.widget.RelativeLayout//android.widget.TextView')
    _closemanagecontacts=(MobileBy.ID,'com.tencent.wework:id/hc9')
    def goto_deletemember(self,username):
        from test_appium.zuoye2.page.delete_member import DeletePage
        self.swipe_find(username).click()
        return DeletePage(self.driver)
    def goto_addresslist(self):
        self.find_and_click(*self._closemanagecontacts)



