# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:44
@Author  : guoyabing
@FileName: addmember_page.py
@Software: PyCharm
 
'''
# from test_appium.zuoye2.page.editmembe_page import EditMemberPage
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.zuoye2.page.base_page import BasePage


class AddmemberPage(BasePage):
    _addmemberclick = (MobileBy.XPATH, "//*[@text='手动输入添加']")
    _back = (MobileBy.ID, "com.tencent.wework:id/hbs")

    def click_addmember_menual(self):
        # click手动输入添加
        self.find_and_click(*self._addmemberclick)
        from test_appium.zuoye2.page.editmembe_page import EditMemberPage
        return EditMemberPage(self.driver)

    def click_back(self):
        from test_appium.zuoye2.page.addresslist_page import AddressListPage
        # 点击回退回到通讯录列表页面
        self.find_and_click(*self._back)
        return AddressListPage(self.driver)

    def get_result(self):
        # result=self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成功']").get_attribute('text')
        result = self.get_toast_text()
        return result
