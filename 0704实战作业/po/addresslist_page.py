# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:42
@Author  : guoyabing
@FileName: addresslist_page.py
@Software: PyCharm
 
'''
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.zuoye2.page.addmember_page import AddmemberPage
from test_appium.zuoye2.page.base_page import BasePage
from test_appium.zuoye2.page.managecontacts_paage import ManageContacts


class AddressListPage(BasePage):
    _delete = (MobileBy.ID, "com.tencent.wework:id/hcd")

    def goto_addmember(self):
        # click添加成员
        self.swipe_find('添加成员').click()
        return AddmemberPage(self.driver)

    def goto_managecontacts(self):
        self.find_and_click(*self._delete)
        return ManageContacts(self.driver)

    # def get_member_names(self):
    #     """ 获取通讯录所有成员名字 """
    #     member_names = [ele.text for ele in self.finds(*self._members_ele) if ele.text != '添加成员']
    #     print(f'member_names is {member_names}')
    #     return member_names
