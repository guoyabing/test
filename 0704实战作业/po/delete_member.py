# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/6 16:59
@Author  : guoyabing
@FileName: delete_member.py
@Software: PyCharm
 
'''
from appium.webdriver.common.mobileby import MobileBy

from test_appium.zuoye2.page.base_page import BasePage



class DeletePage(BasePage):
    _addmemberclick = (MobileBy.XPATH, "//*[@text='手动输入添加']")
    _delete = (MobileBy.XPATH, "//*[@text='删除成员']")
    def delete_member(self):
        from test_appium.zuoye2.page.sure_or_cancel import SureOrCancel
        # 编辑页面点击删除
        self.find_and_click(*self._delete)
        return SureOrCancel(self.driver)

