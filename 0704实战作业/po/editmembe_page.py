# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:46
@Author  : guoyabing
@FileName: editmembe_page.py
@Software: PyCharm
 
'''
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.zuoye2.page.addmember_page import AddmemberPage
from test_appium.zuoye2.page.base_page import BasePage


class EditMemberPage(BasePage):
    _username=(MobileBy.ID, "com.tencent.wework:id/b09")
    _phone=(MobileBy.ID, "com.tencent.wework:id/f7y")
    _save=(MobileBy.ID, "com.tencent.wework:id/ad2")
    def edit_member(self,username,phone):
        #input name
        #input phone
        #click 保存
        self.find_and_sendkeys(*self._username,username)
        self.find_and_sendkeys(*self._phone,phone)
        self.find_and_click(*self._save)

        return AddmemberPage(self.driver)
