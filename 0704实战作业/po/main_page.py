# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:37
@Author  : guoyabing
@FileName: main_page.py
@Software: PyCharm
 
'''
#主页面
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.zuoye2.page.addresslist_page import AddressListPage
from test_appium.zuoye2.page.base_page import BasePage


class MainPage(BasePage):
    _addresslist_element=(MobileBy.XPATH, "//*[@text='通讯录']")
    def goto_addresslist(self):
        #click通讯录
        self.find_and_click(*self._addresslist_element)
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        return AddressListPage(self.driver)