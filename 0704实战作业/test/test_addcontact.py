# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/5 15:52
@Author  : guoyabing
@FileName: test_addcontact.py
@Software: PyCharm
 
'''
import allure
from faker import Faker

from test_appium.zuoye2.page.app import App

@allure.feature("企业微信添加删除联系人")
class TestContact:
    def setup_class(self):
        self.fake = Faker('zh_CN')
        self.app = App()

    def setup(self):
        # 启动应用
        # self.app=App()
        # 执行之后test_addcontact1 执行也会输出driver==None ，这时需要把这句拿出去，不能在setup中，不然没吃会创建一个新的实例
        self.main = self.app.start().goto_main()

    def teardown(self):
        # self.app.quit()
        # self.app.restart()
        self.app.back(5)  # 根据自己的层级来，跳出来

    def tesrdown_class(self):
        self.app.quit()

    @allure.story("添加联系人")
    def test_addcontact(self):
        username = self.fake.name()
        phone = self.fake.phone_number()
        # 进入主页 入口
        result = self.main.goto_addresslist().goto_addmember(). \
            click_addmember_menual().edit_member(username, phone).get_result()
        assert '添加成功' == result


    @allure.story("删除联系人")
    def test_deletecontact(self):
        # username = self.fake.name()
        # phone = self.fake.phone_number()
        username="何畅"
        # 进入主页 入口
        self.main.goto_addresslist().goto_managecontacts().goto_deletemember(username).delete_member(). \
            sure_or_cancel()

