# -*- coding: utf-8 -*-

'''
@Time    : 2021/7/2 15:02
@Author  : guoyabing
@FileName: test_daka.py
@Software: PyCharm

 前提条件
        已登录状态（ noReset=True）
        打卡用例：
        1、打开【企业微信】应用
        2、进入【工作台】
        3、点击【打卡】
        4、选择【外出打卡】tab
        5、点击【第N次打卡】
        6、验证【外出打卡成功】
        7、退出【企业微信】应用
        :return:
'''
from time import sleep

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.common.exceptions import NoSuchElementException
from faker import Faker


class TestWeiXin:
    def setup_class(self):
        self.fake = Faker('zh_CN')

    def setup(self):
        # 资源准备，打开应用
        caps = {}
        caps["platformName"] = "Android"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        caps["deviceName"] = "hogwarts"
        caps["noReset"] = "true"
        # 只有动态页面才需要设置这个时间
        caps["settings[waitForIdleTimeout"] = 0

        # 至关重要的一行  与appium 服务建立连接，并传递一个caps 字典对象
        # self.创建一个实例，要用到driver
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        # 隐式等待 5s动态得等待元素出现，如果5s之内都没有找到就报异常
        self.driver.implicitly_wait(5)

    def trardown(self):
        # 资源的回收
        self.driver.quit()

    def swip_find(self, text, num=5):
        # 滑动找元素
        # while True:
        self.driver.implicitly_wait(1)
        for i in range(num):

            try:
                element = self.driver.find_element(MobileBy.XPATH, f"//*[@text='{text}']")
                self.driver.implicitly_wait(5)
                return element
            except:
                print("未找到")
                size = self.driver.get_window_size()
                width = size['width']
                height = size['height']

                start_x = width / 2
                start_y = height * 0.8
                end_x = start_x
                end_y = height * 0.3
                duration = 1000

                self.driver.swipe(start_x, start_y, end_x, end_y)
            if i == num - 1:
                # 优化隐式等待，提高查找效率
                self.driver.implicitly_wait(5)
                raise NoSuchElementException("找了{i}次，没找到")

    def test_addcontact(self):

        # username = "111"
        # phone = "13300000001"
        username=self.fake.name()
        phone=self.fake.phone_number()

        self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        self.swip_find("添加成员").click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
        self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'姓名')]/../*[@text='必填']").send_keys(username)
        # 或用class定位self.driver.find_element(MobileBy.ID, "//*[contains(@text,'姓名')]/../android.widget.EditText")").send_keys(username)
        # self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/b09").send_keys(username) #com.tencent.wework:id/b09这样随机的ID定位也不建议使用，会根据设备变化可能
        # 或用下面这个
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='必填']")[0].send_keys(username)
        # 没有问题，但是又很多必填的话，中间加了很多会错乱，不建议使用
        self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText").send_keys(
            phone)
        # 查找手机对应的输入框的另一种写法 //*[contains(@text,'手机')]/..//*[@text='必填']
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='必填']")[1].send_keys(username)--不建议
        self.driver.find_element(MobileBy.XPATH, "//*[@text='保存']").click()
        # 断言
        sleep(1)
        # page_source可以打印页面的xml布局
        print(self.driver.page_source)
        result = self.driver.find_element(MobileBy.XPATH, "//*[@class='android.widget.Toast']").get_attribute('text')
        assert '添加成功' == result
        self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/hbs").click()
        self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/hcd").click()
        self.swip_find(username).click()
        self.swip_find("删除成员").click()
        # self.do_screenshot('删除成员确认弹窗')
        self.driver.find_element(MobileBy.XPATH, "//*[@text='确定']").click()



